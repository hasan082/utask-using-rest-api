# UTask - Ultimate Task Management App

Welcome to UTask, the ultimate task management app built with Flutter. UTask empowers users to effortlessly create, organize, and track tasks, enhancing productivity and organization in a user-friendly manner.

## Screenshots


#### Authentication UI
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-1.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-2.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-4.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-3.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-5.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-6.png" width="160" />


#### Task Management UI
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-7.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-8.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-11.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-12.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-9.png" width="160" />
<img src="https://gitlab.com/hasan082/utask-using-rest-api/-/raw/main/screen-10.png" width="160" />


## Features of UTask

- **Task Management:** Seamlessly create tasks by adding titles, descriptions, due dates, and priority levels. Organize tasks into different categories: New, In Progress, Completed, and Cancelled. Edit task details, mark tasks as complete, or cancel them as needed.

- **User Authentication:** Enjoy secure user authentication using email and password credentials. Prevent duplicate registrations by verifying email uniqueness during sign-up. Easily register new accounts or log in to existing ones. Verify email addresses for account confirmation.

- **Profile Management:** Update user profile details, including name, email, profile picture, and password. Choose profile pictures from device uploads or use default avatars.

- **REST API Integration:** Seamlessly interact with the backend server using RESTful APIs. Perform comprehensive CRUD (Create, Read, Update, Delete) operations on tasks and user data via dedicated API endpoints.

- **Image Upload for User Profile:** Allow users to upload images when creating or updating tasks. View and manage task images directly from the task details screen.

- **Task Progress Tracking:** Monitor tasks in progress, keeping an eye on status and completion percentage. Customize task completion rates and gain insights into overall progress.

- **Task Completion and Cancellation:** Effortlessly mark tasks as completed or cancelled. Access separate screens for viewing completed and cancelled tasks, enabling quick reference.

- **User Interface Animation:** Elevate user experience with smooth page transition animations. Utilize custom page route animations to create a polished and engaging journey.

- **Data Persistence:** Implement robust data persistence mechanisms to store user preferences, task details, and app data locally on devices. Utilize techniques such as SQLite or shared preferences for optimal performance.

- **User Experience Enhancements:** Foster intuitive design with seamless navigation and interaction. Provide user feedback and error handling to enhance the overall experience.

- **Responsive Design:** Ensure UTask adapts seamlessly to different screen sizes and orientations, optimizing both mobile phones and tablets.

- **Performance Optimization:** Optimize app performance for smooth operation. Minimize loading times and resource usage for an uninterrupted user experience.

- **Security:** Enforce robust security measures to protect user data and privacy. Utilize encryption and secure authentication protocols to safeguard sensitive information.

## Getting Started

### Prerequisites

Ensure Flutter is installed on your machine. For installation instructions, refer to the official [Flutter website](https://flutter.dev/docs/get-started/install).

### Installation

Follow these steps to set up UTask:

1. Clone this repository to your local machine:

```bash
git clone https://gitlab.com/hasan082/utask-using-rest-api.git
```

2. Navigate to the project folder:

```bash
cd utask
```

3. Install dependencies:

```bash
flutter pub get
```

### How to Run

Connect your device/emulator and run the app using the following command:

```bash
flutter run
```

### Used Packages

UTask integrates the following packages to enhance functionality:

- `flutter_svg: ^2.0.7`: Display SVG images.
- `pin_code_fields: ^8.0.1`: Implement PIN code fields.
- `http: ^1.1.0`: Make HTTP requests to the backend server.
- `shared_preferences: ^2.2.0`: Support local data persistence and user information storage.
- `image_picker: ^1.0.1`: Enable image selection from device.
- `path_provider: ^2.0.15`: Access device's file system paths.
- `permission_handler: ^10.4.3`: Manage runtime permissions for various device features.

## Directory Structure

The UTask directory structure is organized as follows:

```
utask/
  ├── assets/
  │   ├── images/
  │   │   ├── bg.svg
  │   │   ├── logo_black.jpg
  │   │   ├── logo_white.jpg
  ├── lib/
  │   ├── api_data/
  │   │   ├── api_services/
  │   │   │   ├── network_service_handler.dart
  │   ├── models/
  │   │   ├── login_response_model.dart
  │   │   ├── network_services_response.dart
  │   │   ├── task_list_models.dart
  │   │   ├── task_overview_models.dart
  │   ├── utils/
  │   │   ├── add_new_task_utils.dart
  │   │   ├── authentication_utils.dart
  │   │   ├── data_utils.dart
  ├── UI/
  │   ├── Screen/
  │   │   ├── auth/
  │   │   │   ├── forget_password.dart
  │   │   │   ├── login_screen.dart
  │   │   │   ├── otp_verification_screen.dart
  │   │   │   ├── resetverifiction_screen.dart
  │   │   │   ├── signup_screen.dart
  │   │   │   ├── update_profile.dart
  │   │   ├── add_task_screen.dart
  │   │   ├── bottom_nav_home_screen.dart
  │   │   ├── splash_screen.dart
  │   │   ├── task_cancelled.dart
  │   │   ├── task_completed.dart
  │   │   ├── task_new.dart
  │   │   └── task_progress.dart
  │   ├── widgets/
  │   │   ├── edit_task_widget.dart
  │   │   ├── page_route_animation_widget.dart
  │   │   ├── profile_banner_widget.dart
  │   │   ├── screen_bg.dart
  │   │   ├── task_overview_widget.dart
  │   │   ├── update_task_status_widget.dart
  ├── Utils/
  │   ├── assets_utils.dart
  │   ├── task_status_utils.dart
  ├── app.dart
  ├── main.dart

```

## Contributors

- [Md Hasanuzzaman](https://gitlab.com/hasan082)

## Special Thanks

A heartfelt thank you to the following individuals for their guidance, support, and contributions during the development of UTask:

- [RABBIL HASAN](https://rabbil.com/)
- [Md Rafat J. M.](https://www.linkedin.com/in/rafatjamadermaraz/)

Their expertise and insights have been instrumental in making UTask a reality.

## License

This project is licensed under the MIT License. Refer to the [LICENSE](https://opensource.org/license/mit/) file for details.