enum TaskStatus {
  newTask,
  progress,
  completed,
  canceled,
}
