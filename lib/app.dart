import 'package:flutter/material.dart';
import 'package:itask_complete_project/ui/screens/add_task_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/otp_verification_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/reset_password_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/signup_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/update_profile.dart';
import 'package:itask_complete_project/ui/screens/bottom_nav_home_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/login_screen.dart';
import 'package:itask_complete_project/ui/screens/splash_screen.dart';
import 'package:itask_complete_project/ui/screens/auth/forget_password.dart';
import 'package:itask_complete_project/ui/screens/task_new.dart';
import 'package:itask_complete_project/ui/widgets/page_route_animation_widget.dart';

class UTask extends StatelessWidget {
  static final GlobalKey<NavigatorState> globalKey = GlobalKey<NavigatorState>();
  const UTask({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: UTask.globalKey,
      debugShowCheckedModeBanner: false,
      title: 'UTask',
      home: const SplashScreen(),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/login':
            return CustomPageRoute(
              page: const LoginScreen(),
              pageSettings: settings,
              axis: SlideDirection.rightToLeft,
            );
          case '/resetPass':
            return CustomPageRoute(
              page: const ResetPasswordScreen(),
              pageSettings: settings,
              axis: SlideDirection.rightToLeft,
            );
          case '/otp':
            return CustomPageRoute(
              page: const OtpVerificationScreen(),
              pageSettings: settings,
              axis: SlideDirection.rightToLeft,
            );
          case '/forgetPass':
            return CustomPageRoute(
                page: const ForgetPassword(),
                pageSettings: settings,
                axis: SlideDirection.rightToLeft);
          case '/signUp':
            return CustomPageRoute(
                page: const SignUpScreen(),
                pageSettings: settings,
                axis: SlideDirection.rightToLeft);
          case '/bottomHome':
            return CustomPageRoute(
                page: const BottomNavHomeScreen(),
                pageSettings: settings,
                axis: SlideDirection.rightToLeft);
          case '/addTask':
            return CustomPageRoute(
                page: const AddTaskScreen(),
                pageSettings: settings,
                axis: SlideDirection.rightToLeft);
          case '/updateProfile':
            return CustomPageRoute(
              page: const UpdateProfile(),
              pageSettings: settings,
              axis: SlideDirection.rightToLeft,
              routeName: settings.name,
            );
          case '/newTaskAdd':
            return CustomPageRoute(
              page: const TaskNew(),
              pageSettings: settings,
              axis: SlideDirection.rightToLeft,
            );
          default:
            return CustomPageRoute(
                page: const LoginScreen(),
                pageSettings: settings,
                axis: SlideDirection.rightToLeft);
        }
      },
      theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.green,
          textTheme: const TextTheme(
            bodySmall: TextStyle(
              fontSize: 14,
              color: Colors.black,
              fontWeight: FontWeight.normal,
            ),
            bodyMedium: TextStyle(
              fontSize: 16,
              color: Colors.black,
              fontWeight: FontWeight.normal,
            ),
            bodyLarge: TextStyle(
              fontSize: 18,
              color: Colors.black,
              fontWeight: FontWeight.normal,
            ),
            titleSmall: TextStyle(
              fontSize: 20,
              color: Colors.black,
              fontWeight: FontWeight.w500,
            ),
            titleMedium: TextStyle(
              fontSize: 22,
              color: Colors.black,
              fontWeight: FontWeight.w500,
            ),
            titleLarge: TextStyle(
              fontSize: 24,
              color: Colors.black,
              fontWeight: FontWeight.w500,
            ),
          ),
          iconTheme: const IconThemeData(
            size: 24,
          ),
          inputDecorationTheme: InputDecorationTheme(
            filled: true,
            fillColor: Colors.white,
            hintStyle: TextStyle(fontSize: 17, color: Colors.grey.shade400),
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 16,
            ),
            border: const OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
          ),
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              padding: const EdgeInsets.symmetric(vertical: 9),
              elevation: 3,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5)),
            ),
          )).copyWith(
        pageTransitionsTheme: const PageTransitionsTheme(
          builders: <TargetPlatform, PageTransitionsBuilder>{
            TargetPlatform.android: ZoomPageTransitionsBuilder(
                allowEnterRouteSnapshotting: true, allowSnapshotting: true),
          },
        ),
      ),
      darkTheme: ThemeData(brightness: Brightness.dark),
      themeMode: ThemeMode.light,
    );
  }
}
