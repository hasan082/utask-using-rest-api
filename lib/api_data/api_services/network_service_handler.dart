import 'dart:convert';
import 'dart:async';
import 'dart:developer';
import 'package:http/http.dart';
import 'package:itask_complete_project/api_data/utils/authentication_utils.dart';
import '../../app.dart';
import '../models/network_services_response.dart';
import 'package:flutter/material.dart';

class NetworkServiceHandler {

  Future<NetworkResponse> getRequest(String url) async {
    try{
      Response response = await get(Uri.parse(url), headers: {
        'Content-type': 'application/json',
        'token': AuthUtility.userInfo.token.toString()
      });
      if(response.statusCode == 200){
        return NetworkResponse(true, response.statusCode, jsonDecode(response.body));
      }else {
        return NetworkResponse(false, response.statusCode, null);
      }
    }catch(error){
      log(error.toString());
    }
    return NetworkResponse(false, -1, null);
  }

  Future<NetworkResponse> postRequest(String url, Map<String, dynamic> body,{bool isLogin = false}) async {
    try{
      Response response = await post(Uri.parse(url),headers: {
        'Content-Type': 'application/json',
        'token': AuthUtility.userInfo.token.toString()
      }, body: jsonEncode(body));
      if(response.statusCode == 200){
        return NetworkResponse(true, response.statusCode, jsonDecode(response.body));
      }else if (response.statusCode == 401){
        if(isLogin==false){
          backToLogin();
        }
      }else {
        return NetworkResponse(false, response.statusCode, null);
      }
    }catch(error){
      log(error.toString());
    }
    return NetworkResponse(false, -1, null);
  }

  //ToDo==== I have fixed below function


  Future<void> backToLogin() async {
    await AuthUtility.clearUserInfo();
    Navigator.pushNamedAndRemoveUntil(UTask.globalKey.currentContext!, '/login', (route) => false);
  }




}