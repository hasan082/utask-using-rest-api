import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:itask_complete_project/ui/widgets/profile_banner_widget.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import '../../../api_data/api_services/network_service_handler.dart';
import '../../../api_data/models/network_services_response.dart';
import '../../../api_data/utils/authentication_utils.dart';
import '../../../api_data/utils/data_utils.dart';

class UpdateProfile extends StatefulWidget {
  final bool? isuUpdateScreen;

  const UpdateProfile({Key? key, this.isuUpdateScreen}) : super(key: key);

  @override
  State<UpdateProfile> createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  bool obscureState = true;
  bool isLoading = false;
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _firstNameCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.firstName ?? '');
  final TextEditingController _lastNameCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.lastName ?? '');
  final TextEditingController _mobileCtrl =
      TextEditingController(text: AuthUtility.userInfo.data?.mobile ?? '');
  final TextEditingController _passwordCtrl = TextEditingController();
  XFile? imageFile;
  final ImagePicker _picker = ImagePicker();

  void selectImage() {
    _picker.pickImage(source: ImageSource.gallery).then((xFile) {
      if (xFile != null) {
        imageFile = xFile;
        if (mounted) {
          setState(() {});
        }
      }
    });
  }

  Future<void> pickImage() async {
    if (mounted) {
      await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Select Image Source'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  selectImage();
                },
                child: const Text('Gallery'),
              ),
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                  //todo
                },
                child: const Text('Camera'),
              ),
            ],
          );
        },
      );
    }
  }

  Future<void> updateUserProfile() async {

    isLoading = true;
    if (mounted) {
      setState(() {});
    }

    Map<String, dynamic> requestBody = {
      "email": AuthUtility.userInfo.data!.email,
      "firstName": _firstNameCtrl.text.trim(),
      "lastName": _lastNameCtrl.text.trim(),
      "mobile": _mobileCtrl.text.trim(),
    };

    if (_passwordCtrl.text.trim().isNotEmpty) {
      requestBody["password"] = _passwordCtrl.text;
    }

    if (imageFile != null) {
      requestBody["photo"] = imageFile!.path;
    } else {
      requestBody["photo"] = AuthUtility.userInfo.data!.photo;
    }

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.profileUpdateUrls, requestBody);

    isLoading = false;
    if (mounted) {
      setState(() {});
    }

    if (response.isSuccess) {
      AuthUtility.userInfo.data!.firstName = requestBody["firstName"];
      AuthUtility.userInfo.data!.lastName = requestBody["lastName"];
      AuthUtility.userInfo.data!.mobile = requestBody["mobile"];
      AuthUtility.userInfo.data!.photo = requestBody["photo"];

      await AuthUtility.saveUserInfo(AuthUtility.userInfo);

      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Profile updated')),
        );
        Navigator.pushNamedAndRemoveUntil(
            context,
            '/bottomHome',
            arguments: imageFile?.path,
            (route) => false);
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
              content: Text('Profile update failed. Please try again.')),
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ProfileBannerWidget(
        isuUpdateScreen: true,
      ),
      body: ScreenBg(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Form(
                key: _formKey,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Update Profile',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 20),
                      InkWell(
                        onTap: () {
                          pickImage();
                        },
                        child: Container(
                          width: double.maxFinite,
                          decoration: const BoxDecoration(
                            color: Colors.white,
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 16),
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                  color: Colors.green,
                                ),
                                width: 90,
                                child: const Text(
                                  'Photo',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              const SizedBox(width: 15),
                              Visibility(
                                visible: imageFile != null,
                                replacement: const Text('Upload Image'),
                                child: SizedBox(
                                  width: 180,
                                  child: Text(
                                    imageFile?.name ?? ' ',
                                    style: const TextStyle(
                                      overflow: TextOverflow.ellipsis,
                                      fontSize: 14,
                                      // Adjust the font size as needed
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        readOnly: true,
                        initialValue: AuthUtility.userInfo.data?.email ?? '',
                        decoration:
                        const InputDecoration(hintText: 'Email'),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _firstNameCtrl,
                        decoration:
                            const InputDecoration(hintText: 'First Name'),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _lastNameCtrl,
                        decoration:
                            const InputDecoration(hintText: 'Last Name'),
                      ),
                      TextFormField(
                        controller: _mobileCtrl,
                        decoration: const InputDecoration(hintText: 'Mobile'),
                      ),
                      const SizedBox(height: 10),
                      TextFormField(
                        controller: _passwordCtrl,
                        obscureText: obscureState,
                        decoration: InputDecoration(
                          hintText: 'Password',
                          suffixIcon: InkWell(
                            onTap: () {
                              setState(() {
                                obscureState = !obscureState;
                              });
                            },
                            child: Icon(!obscureState
                                ? Icons.visibility
                                : Icons.visibility_off),
                          ),
                        ),
                        validator: (String? value) {
                          if (value?.isNotEmpty == true && value!.length < 6) {
                            return 'Password must have a minimum of 6 characters';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 10),
                      SizedBox(
                        width: double.maxFinite,
                        child: Visibility(
                          visible: !isLoading,
                          replacement: const Center(child: CircularProgressIndicator(),),
                          child: ElevatedButton(
                            onPressed: () {
                              if (!_formKey.currentState!.validate()) {
                                return;
                              }
                              updateUserProfile();
                            },
                            child: const Icon(Icons.arrow_circle_right_outlined),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
