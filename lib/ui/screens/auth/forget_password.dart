import 'package:flutter/material.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import '../../../api_data/api_services/network_service_handler.dart';
import '../../../api_data/models/network_services_response.dart';
import '../../../api_data/utils/data_utils.dart';

class ForgetPassword extends StatefulWidget {
  const ForgetPassword({Key? key}) : super(key: key);

  @override
  State<ForgetPassword> createState() =>
      _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  bool isLoading = false;
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _emailCtrl = TextEditingController();




  Future<void> forgetPassword() async {

    isLoading = true;
    if(mounted){
      setState(() {

      });
    }

    final NetworkResponse response = await NetworkServiceHandler().getRequest(Urls.forGetPasswordUrl(_emailCtrl.text.trim()));


    isLoading = false;
    setState(() {});

    if(response.isSuccess && response.body?['status'] == 'success'){
      if(mounted){
        Navigator.pushNamed(context, '/otp', arguments: _emailCtrl.text.trim());
        if(mounted){
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Check your email for OTP')));
        }
        setState(() {});
      }
    }else {
      if(mounted){
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Email not registered')));
      }

    }


  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Your Email Address',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      'A 6 digit verification code sent to your email address',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _emailCtrl,
                      decoration: const InputDecoration(
                        hintText: 'Email',
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (String? value) {
                        final emailRegex =
                        RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$');
                        if (value?.isEmpty ?? true) {
                          return 'Enter your Email';
                        } else if (!emailRegex.hasMatch(value!)) {
                          return 'Enter a valid email address';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 15),
                    SizedBox(
                      width: double.infinity,
                      child: Visibility(
                        visible: !isLoading,
                        replacement: const Center(child: CircularProgressIndicator(),),
                        child: Visibility(
                          visible: !isLoading,
                          replacement: const Center(child: CircularProgressIndicator(),),
                          child: ElevatedButton(
                            onPressed: () {
                              if (!_formKey.currentState!.validate()) {
                                return;
                              }
                              forgetPassword();
                            },
                            child: const Icon(Icons.arrow_forward_ios),
                          ),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
