import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:itask_complete_project/api_data/api_services/network_service_handler.dart';
import 'package:itask_complete_project/api_data/models/login_response_model.dart';
import 'package:itask_complete_project/api_data/models/network_services_response.dart';
import 'package:itask_complete_project/api_data/utils/authentication_utils.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';
import '../../widgets/screen_bg.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool obscureState = true;
  bool isLoginProgress = false;
  final TextEditingController _emailCtrl = TextEditingController();
  final TextEditingController _passwordCtrl = TextEditingController();

  Future<void> userLogIn() async {
    isLoginProgress = true;
    if(mounted){
      setState(() {

      });
    }
    Map<String, dynamic> responseBody = {
      "email": _emailCtrl.text.trim(),
      "password": _passwordCtrl.text,
    };
    final NetworkResponse response =
        await NetworkServiceHandler().postRequest(Urls.loginUrls, responseBody, isLogin: true);
    try{
      if (response.isSuccess) {
        LoginResponseModel loginResponseModel = LoginResponseModel.fromJson(response.body!);
        await AuthUtility.saveUserInfo(loginResponseModel);
        isLoginProgress = false;
        if(mounted){
          setState(() {});
        }
        if (mounted) {
          Navigator.pushNamedAndRemoveUntil(context, '/bottomHome', (route) => false);
          // Navigator.pushNamed(context, '/bottomHome');
        }
      } else {
        isLoginProgress = false;
        if(mounted){
          setState(() {});
        }
        if (mounted) {
          ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Incorrect Credentials')));
        }
      }
    }catch(e){
      log(e.toString());
    }

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Get Started with',
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _emailCtrl,
                    decoration: const InputDecoration(hintText: 'Email'),
                    validator: (String? value) {
                      final emailRegex =
                          RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$');
                      if (value?.isEmpty ?? true) {
                        return 'Enter your Email';
                      } else if (!emailRegex.hasMatch(value!)) {
                        return 'Enter a valid email address';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  TextFormField(
                    controller: _passwordCtrl,
                    obscureText: obscureState,
                    // Use the negation to show/hide password
                    decoration: InputDecoration(
                        hintText: 'Password',
                        suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              obscureState = !obscureState; // Toggle isVisible
                            });
                          },
                          child: Icon(!obscureState
                              ? Icons.visibility
                              : Icons.visibility_off),
                        )
                        //
                        ),
                    validator: (String? value) {
                      if (value!.isEmpty || value.length < 6) {
                        return 'Password must have a minimum of 6 characters';
                      }
                      return null;
                    },
                  ),
                  const SizedBox(height: 15),
                  SizedBox(
                    width: double.maxFinite,
                    child: Visibility(
                      visible: !isLoginProgress,
                      replacement: const Center(child: CircularProgressIndicator(),),
                      child: ElevatedButton(
                        onPressed: () {
                          userLogIn();
                        },
                        child: const Icon(Icons.arrow_forward_ios),
                      ),
                    ),
                  ),
                  const SizedBox(height: 40),
                  Center(
                    child: InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, '/forgetPass');
                      },
                      child: const Text('Forgot Password?'),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Center(child: Text('Don\'t have an account?')),
                      TextButton(
                        onPressed: () {
                          Navigator.pushNamed(context, '/signUp');
                        },
                        child: const Text('Sign up'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

