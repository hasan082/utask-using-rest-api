

import 'package:flutter/material.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../../../api_data/api_services/network_service_handler.dart';
import '../../../api_data/models/network_services_response.dart';
import '../../../api_data/utils/data_utils.dart';

class OtpVerificationScreen extends StatefulWidget {

  const OtpVerificationScreen({Key? key}) : super(key: key);

  @override
  State<OtpVerificationScreen> createState() => _OtpVerificationScreenState();
}

class _OtpVerificationScreenState extends State<OtpVerificationScreen> {
  final TextEditingController _otpCtrl = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }



  Future<void> otpVerifyMethod(String email) async {

    isLoading = true;
    if(mounted){
      setState(() {

      });
    }

    final NetworkResponse response = await NetworkServiceHandler().getRequest(Urls.otpVerifyUrl(email, _otpCtrl.text));


    isLoading = false;
    if(mounted){
      setState(() {});
    }



    if(response.isSuccess && response.body?['status'] == 'success'){

      if(mounted){
        Navigator.pushNamed(context, '/resetPass', arguments: {
          'email': email,
          'otp': _otpCtrl.text,
        });

        if(mounted){
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('OTP Verified Successfully')));
        }

      }
    }else {
      if(mounted){
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Wrong OTP')));
      }
      isLoading = false;
      if(mounted){
        setState(() {});
      }
    }


  }






  @override
  Widget build(BuildContext context) {
    final emailArguments = ModalRoute.of(context)!.settings.arguments as String;
    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Pin Verification',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      '6 digit verification code send to your email',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    PinCodeTextField(
                      controller: _otpCtrl,
                      length: 6,
                      obscureText: false,
                      animationType: AnimationType.fade,
                      pinTheme: PinTheme(
                          shape: PinCodeFieldShape.box,
                          borderRadius: BorderRadius.circular(5),
                          fieldHeight: 50,
                          fieldWidth: 40,
                          activeFillColor: Colors.white,
                          selectedColor: Colors.white,
                          selectedFillColor: Colors.green.shade500,
                          inactiveFillColor: Colors.white54),
                      animationDuration: const Duration(milliseconds: 300),
                      backgroundColor: Colors.blue.shade50,
                      enableActiveFill: true,
                      onCompleted: (v) {},
                      beforeTextPaste: (text) {
                        return true;
                      },
                      appContext: context,
                    ),
                    const SizedBox(height: 15),
                    SizedBox(
                      width: double.infinity,
                      child: Visibility(
                        visible: !isLoading,
                        replacement: const Center(child: CircularProgressIndicator(),),
                        child: ElevatedButton(
                          onPressed: () {
                            if (!_formKey.currentState!.validate()) {
                              return;
                            }
                            otpVerifyMethod(emailArguments);
                          },
                          child: const Text('Verify'),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}


