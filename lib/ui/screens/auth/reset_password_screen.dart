import 'package:flutter/material.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import 'package:itask_complete_project/api_data/api_services/network_service_handler.dart';
import 'package:itask_complete_project/api_data/models/network_services_response.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';


class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  State<ResetPasswordScreen> createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  bool obscureState = true,
      isLoading = false,
      confirmObscureState = true,
      passwordMatch = true;
  final TextEditingController _passwordCtrl = TextEditingController();
  final TextEditingController _confirmPasswordCtrl = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey();

  void checkPasswordMatch() {
    final String password = _passwordCtrl.text;
    final String confirmPassword = _confirmPasswordCtrl.text;
    setState(() {
      passwordMatch = password == confirmPassword;
    });
  }

  Future<void> resetPasswordMethod(String email, String otp) async {
    isLoading = true;
    setState(() {});


    Map<String, dynamic> requestBody = {
      "email": email,
      "OTP": otp,
      "password": _confirmPasswordCtrl.text
    };

    final NetworkResponse response =
        await NetworkServiceHandler().postRequest(Urls.resetPasswordUrl, requestBody);


    isLoading = false;
    if (mounted) {
      setState(() {});
    }

    if (response.isSuccess) {
      if (mounted) {
        Navigator.pushNamedAndRemoveUntil(context, '/login', (route) => false);
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Password reset Successfully')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Password reset failed')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final Map<String, dynamic>? optEmailArguments =
        ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>?;

    return Scaffold(
      body: ScreenBg(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Set Password',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      'Minimum length of password is 8 characters with letter and number combination',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _passwordCtrl,
                      obscureText: obscureState,
                      onChanged: (_) => checkPasswordMatch(),
                      decoration: InputDecoration(
                        hintText: 'Password',
                        errorText: passwordMatch ? null : "Passwords don't match",
                        suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              obscureState = !obscureState;
                            });
                          },
                          child: Icon(!obscureState
                              ? Icons.visibility
                              : Icons.visibility_off),
                        ),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password cannot be empty';
                        } else if (value.length < 6) {
                          return 'Password must have a minimum of 6 characters';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 15),
                    TextFormField(
                      controller: _confirmPasswordCtrl,
                      obscureText: confirmObscureState,
                      onChanged: (_) => checkPasswordMatch(),
                      decoration: InputDecoration(
                        hintText: 'Confirm Password',
                        errorText: passwordMatch ? null : "Passwords don't match",
                        suffixIcon: InkWell(
                          onTap: () {
                            setState(() {
                              confirmObscureState = !confirmObscureState;
                            });
                          },
                          child: Icon(!confirmObscureState
                              ? Icons.visibility
                              : Icons.visibility_off),
                        ),
                      ),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Confirm Password cannot be empty';
                        } else if (value.length < 6) {
                          return 'Confirm Password must have a minimum of 6 characters';
                        }
                        return null;
                      },
                    ),
                    const SizedBox(height: 15),
                    SizedBox(
                      width: double.infinity,
                      child: Visibility(
                        visible: !isLoading,
                        replacement: const Center(child: CircularProgressIndicator(),),
                        child: ElevatedButton(
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              if (optEmailArguments != null) {
                                String email = optEmailArguments['email'];
                                String otp = optEmailArguments['otp'];
                                resetPasswordMethod(email, otp);
                              }
                            } else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                const SnackBar(content: Text('Invalid input')),
                              );
                            }
                          },
                          child: const Text('Confirm'),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('Have an account?',
                            style: Theme.of(context).textTheme.bodyMedium),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: const Text('Sign in'),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
