import 'package:flutter/material.dart';
import 'package:itask_complete_project/api_data/api_services/network_service_handler.dart';
import 'package:itask_complete_project/api_data/models/network_services_response.dart';
import 'package:itask_complete_project/ui/widgets/screen_bg.dart';
import '../../api_data/utils/data_utils.dart';
import '../widgets/profile_banner_widget.dart';


class AddTaskScreen extends StatefulWidget {
  const AddTaskScreen({Key? key}) : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  final TextEditingController _titleCtrl = TextEditingController();
  final TextEditingController _descriptionCtrl = TextEditingController();

  bool isTaskAddProgress = false;

  Future<void> addNewTaskMethod() async {
    isTaskAddProgress = true;
    if (mounted) {
      setState(() {});
    }

    Map<String, dynamic> responseBody = {
      "title": _titleCtrl.text.trim(),
      "description": _descriptionCtrl.text.trim(),
      "status": 'New',
    };

    final NetworkResponse response = await NetworkServiceHandler()
        .postRequest(Urls.createNewTaskUrls, responseBody);

    if (response.isSuccess) {
      isTaskAddProgress = false;
      if (mounted) {
        setState(() {});
      }
      _titleCtrl.clear();
      _descriptionCtrl.clear();
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task Added Successfully')));
        Navigator.pushNamedAndRemoveUntil(
            context, '/bottomHome', (route) => false);
      } else {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Task Add Failed')));
      }
    } else {
      isTaskAddProgress = false;
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('API Failed')));
        setState(() {});
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ProfileBannerWidget(),
      body: ScreenBg(
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Add New Task',
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 15),
                      TextFormField(
                        controller: _titleCtrl,
                        decoration: const InputDecoration(hintText: 'Title'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Please enter a title.';
                          }
                          return null; // Return null if the value is valid
                        },
                      ),
                      const SizedBox(height: 15),
                      TextFormField(
                        controller: _descriptionCtrl,
                        maxLines: 5,
                        decoration: const InputDecoration(
                          hintText: 'Description',
                        ),
                      ),
                      const SizedBox(height: 15),
                      SizedBox(
                        width: double.infinity,
                        child: Visibility(
                          visible: !isTaskAddProgress,
                          replacement:
                              const Center(child: CircularProgressIndicator()),
                          child: ElevatedButton(
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                addNewTaskMethod();
                              }
                            },
                            child: const Icon(Icons.arrow_forward_ios),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
