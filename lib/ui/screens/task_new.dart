import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:itask_complete_project/api_data/api_services/network_service_handler.dart';
import 'package:itask_complete_project/api_data/models/network_services_response.dart';
import 'package:itask_complete_project/api_data/models/task_overview_model.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';
import 'package:itask_complete_project/ui/widgets/update_task_status_widget.dart';
import '../../api_data/models/task_list_model.dart';
import '../widgets/task_overview_widget.dart';
import '../widgets/task_single_item_widget.dart';
import '../widgets/edit_task_widget.dart';

class TaskNew extends StatefulWidget {
  const TaskNew({Key? key}) : super(key: key);

  @override
  State<TaskNew> createState() => _TaskNewState();
}

class _TaskNewState extends State<TaskNew> {
  late bool isTaskOverViewPageProgress = false;
  late bool isNewTaskListInProgress = false;
  late bool updateTaskInProgress = false;
  TaskOverviewModel taskOverviewModel = TaskOverviewModel();
  TaskListModel taskListModel = TaskListModel();


  // late String selectedStatus = "New";

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getTaskOverviewData();
      getNewTaskListData();
    });
  }

  Future<void> getTaskOverviewData() async {
    isTaskOverViewPageProgress = true;
    if (mounted) {
      setState(() {});
    }
    final NetworkResponse response =
        await NetworkServiceHandler().getRequest(Urls.taskOverUrl);

  log({response.body?.length}.toString());


  try{
    if (response.isSuccess) {
      taskOverviewModel = TaskOverviewModel.fromJson(response.body!);
      log('${response.body!}');
      isTaskOverViewPageProgress = false;
      if (mounted) {
        setState(() {});
      }
    }else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Overview Data loading failed')));
        isTaskOverViewPageProgress = false;
        if (mounted) {
          setState(() {});
        }
      }
    }
  }catch(e){
    log(e.toString());
  }


  }

  Future<void> getNewTaskListData() async {

    isNewTaskListInProgress = true;
    if (mounted) {
      setState(() {});
    }
    final NetworkResponse response =
        await NetworkServiceHandler().getRequest(Urls.newTaskDataUrl);

    isNewTaskListInProgress = false;
    if (mounted) {
      setState(() {});
    }
    if (response.isSuccess) {
      taskListModel = TaskListModel.fromJson(response.body!);

    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('New Data loading failed')));
        isNewTaskListInProgress = false;
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  Future<void> deleteTask(String taskId) async {
    final NetworkResponse response = await NetworkServiceHandler()
        .getRequest(Urls.deleteTaskStatusUrl(taskId));
    if (response.isSuccess) {
      taskListModel.data!.removeWhere((element) => element.sId == taskId);
      if (mounted) {
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task deleted successfully')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Deletion failed')));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addTask');
        },
        child: const Icon(Icons.add),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: isTaskOverViewPageProgress
                  ? const LinearProgressIndicator()
                  : Row(
                      children: [
                        TaskOverview(
                          taskNumber: getTaskNumberForStatus("New"),
                          overviewStatus: "New",
                        ),
                        TaskOverview(
                          taskNumber: getTaskNumberForStatus("Progress"),
                          overviewStatus: "Progress",
                        ),
                        TaskOverview(
                          taskNumber: getTaskNumberForStatus("Completed"),
                          overviewStatus: "Completed",
                        ),
                        TaskOverview(
                          taskNumber: getTaskNumberForStatus("Cancelled"),
                          overviewStatus: "Cancelled",
                        ),
                      ],
                    ),
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 6),
                child: updateTaskInProgress
                    ? const Center(child: CircularProgressIndicator())
                    : RefreshIndicator(
                        onRefresh: () async {
                          getNewTaskListData();
                        },
                        child: ListView.separated(
                          shrinkWrap: true,
                          physics: const BouncingScrollPhysics(),
                          itemCount: taskListModel.data?.length ?? 0,
                          itemBuilder: (context, index) {
                            return TaskSingleItemWidget(
                              chipBgColor: const Color(0xFF42C1E8),
                              taskData: taskListModel.data![index],
                              onTapDelete: () {
                                deleteTask(taskListModel.data![index].sId!);
                              },
                              onTapStatusChange: () {
                                statusUpdateBottomSheet(taskListModel.data![index]);
                              },
                            );
                          },
                          separatorBuilder: (BuildContext context, int index) {
                            return const SizedBox(
                              height: 10,
                            );
                          },
                        ),
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void editTaskBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return EditTaskBottomSheet(
            task: task,
            onUpdateStatus: () {
              getNewTaskListData();
            },
          );
        });
  }

  void statusUpdateBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, updateState) {
              return UpdateTaskStatus(task: task, onUpdate: (){
                getNewTaskListData();
                getTaskOverviewData();
              });
            },
          );
        });
  }

  int getTaskNumberForStatus(String status) {
    if (taskOverviewModel.data == null) {
      return 0;
    }

    for (var task in taskOverviewModel.data!) {
      if (task.sId == status) {
        return task.sum ?? 0;
      }
    }

    return 0;
  }
}
