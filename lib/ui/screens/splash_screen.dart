import 'package:flutter/material.dart';
import 'package:itask_complete_project/api_data/utils/authentication_utils.dart';
import 'package:itask_complete_project/utils/assets_utils.dart';
import '../widgets/screen_bg.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin{

  late AnimationController _controller;


  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _controller.forward().whenComplete(() async {
      final bool isLoggedIn = await AuthUtility.checkUserLoggedIn();
      if(mounted){
        Navigator.pushNamedAndRemoveUntil(context, isLoggedIn ? '/bottomHome' : '/login', (route) => false);
      }
    });

  }


  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScreenBg(
         child: Center(
           child: Column(
             mainAxisSize: MainAxisSize.max,
             mainAxisAlignment: MainAxisAlignment.center,
             crossAxisAlignment: CrossAxisAlignment.center,
             children: [
               Image.asset(
                 AssetUtils.logoJpgWhite,
                 width: 90,
               ),
               const SizedBox(
                 height: 16,
               ),
               SizedBox(
                   width: 120,
                   child: ClipRRect(
                     borderRadius: BorderRadius.circular(10),
                     child: AnimatedBuilder(
                       animation: _controller,
                       builder: (BuildContext context, Widget? child) {
                         return LinearProgressIndicator(
                           value: _controller.value.toDouble(),
                           minHeight: 4,
                           backgroundColor: const Color(0xFFE8D167),
                           valueColor: const AlwaysStoppedAnimation<Color>(
                             Color(0xFF9E5D1B),
                           ),
                         );
                       },
                     ),
                   ),
               ),
             ],
           ),
         ),
      ),
    );
  }
}
