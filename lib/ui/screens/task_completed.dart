import 'package:flutter/material.dart';
import 'package:itask_complete_project/api_data/utils/data_utils.dart';
import '../../api_data/api_services/network_service_handler.dart';
import '../../api_data/models/network_services_response.dart';
import '../../api_data/models/task_list_model.dart';
import '../widgets/task_single_item_widget.dart';
import '../widgets/update_task_status_widget.dart';

class TaskCompleted extends StatefulWidget {
  const TaskCompleted({Key? key}) : super(key: key);

  @override
  State<TaskCompleted> createState() => _TaskCompletedState();
}

class _TaskCompletedState extends State<TaskCompleted> {
  late bool isLoading = false;
  TaskListModel taskListModel = TaskListModel();


  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getCompletedTaskListData();
    });
  }

  Future<void> getCompletedTaskListData() async {
    isLoading = true;
    if (mounted) {
      setState(() {});
    }
    final NetworkResponse response =
        await NetworkServiceHandler().getRequest(Urls.completedTaskDataUrl);

    if (response.isSuccess) {
      taskListModel = TaskListModel.fromJson(response.body!);
      isLoading = false;
      if (mounted) {
        setState(() {});
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Progress Data loading failed')));
        isLoading = false;
        if (mounted) {
          setState(() {});
        }
      }
    }
  }

  Future<void> deleteTask(String taskId) async {
    final NetworkResponse response = await NetworkServiceHandler()
        .getRequest(Urls.deleteTaskStatusUrl(taskId));
    if (response.isSuccess) {
      taskListModel.data!.removeWhere((element) => element.sId == taskId);
      if (mounted) {
        setState(() {});
        ScaffoldMessenger.of(context).showSnackBar(
            const SnackBar(content: Text('Task deleted successfully')));
      }
    } else {
      if (mounted) {
        ScaffoldMessenger.of(context)
            .showSnackBar(const SnackBar(content: Text('Deletion failed')));
      }
    }
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 10.0, horizontal: 6),
                child: isLoading ? const Center(child: CircularProgressIndicator(),) : RefreshIndicator(
                  onRefresh: () async {
                    getCompletedTaskListData();
                  },
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: const BouncingScrollPhysics(),
                    itemCount: taskListModel.data?.length ?? 0,
                    itemBuilder: (context, index) {
                      return TaskSingleItemWidget(
                        chipBgColor: const Color(0xFF3BBF72),
                        taskData: taskListModel.data![index],
                        onTapDelete: () {
                          deleteTask(taskListModel.data![index].sId!);
                        },
                        onTapStatusChange: () {
                          statusUpdateBottomSheet(
                              taskListModel.data![index]);
                        },
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox(
                        height: 10,
                      );
                    },
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void statusUpdateBottomSheet(TaskData task) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, updateState) {
              return UpdateTaskStatus(
                  task: task,
                  onUpdate: () {
                    getCompletedTaskListData();
                  });
            },
          );
        });
  }


}
